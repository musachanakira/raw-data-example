const port = 3000;

const express         = require('express');
const cors            = require('cors');
const bodyParser      = require('body-parser');
const openapiJSDoc    = require('openapi-jsdoc');
const swaggerUi       = require('swagger-ui-express');

// Initialize openapi-jsdoc -> returns validated OpenAPI spec in json format
const api = openapiJSDoc({
  definition: {
    // info object, see https://swagger.io/specification/#infoObject
    info: {
      title: 'Example app', // required
      version: '1.0.0', // required
      description: 'A sample API for example app',
    },
  },
  // Paths to the API docs
  apis: [
    './handlers/*.js',
    './src/Message/**/*.js',
    './doc/parameters.yaml'
  ],
});

// Initialize app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const corsOptions = {
  origin: `http://locahost:${port}`,
  optionsSuccessStatus: 200,
};
app.use(cors(corsOptions));

// Serve OpenAPI docs
app.get('/v1/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(api);
});
const options = {
  explorer: true,
  swaggerOptions: {
    url: '/v1/swagger.json',
  },
};
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));

app.listen(port, () => {
  console.log('Example app listening at http://localhost:%s/v1/swagger.json', port);
  console.log('Example app listening at http://localhost:%s/api-docs', port);
});
