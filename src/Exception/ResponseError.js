export default class ResponseError extends Error {
  constructor(mesage, statusCode, fileName, lineNumber) {
    super(mesage, fileName, lineNumber);

    this.statusCode = statusCode;
  }
}
