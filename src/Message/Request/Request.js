
export default class Request {
  constructor(body, context, callback) {
    this.body = body;
    this.context = context;
    this.callback = callback;
  }

  getBody() {
    return this.body;
  }

  getCallback() {
    return this.callback;
  }

  getContext() {
    return this.context;
  }
}
