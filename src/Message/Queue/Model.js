/**
 * @openapi
 * components:
 *  schemas:
 *    Model:    # Schema name
 *      type: object
 *      properties:
 *        manufacturerId:
 *          type: string
 *          example: AUD0
 */
export default class Model {
  /**
   *
   * @param {String} manufacturerId
   * @param {String} modelId
   */
  constructor(manufacturerId, modelId) {
    this.manufacturerId = manufacturerId;
    this.modelId = modelId;
  }

  /**
   * @returns {String}
   */
  getManufacturerId() {
    return this.manufacturerId;
  }

  /**
   * @returns {String}
   */
  getModelId() {
    return this.modelId;
  }
}
