/**
 * @openapi
 * components:
 *  schemas:
 *    AllManufacturers:    # Schema name
 *      type: object
 *      properties:
 */
export default class Manufacturer {
  /**
   *
   * @param {String} manufacturerId
   * @param {String} modelId
   */
  constructor(manufacturerId, modelId) {
    this.manufacturerId = manufacturerId;
    this.modelId = modelId;
  }

  /**
   * @returns {String}
   */
  getManufacturerId() {
    return this.manufacturerId;
  }

  /**
   * @returns {String}
   */
  getModelId() {
    return this.modelId;
  }
}
