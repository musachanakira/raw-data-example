/**
 * @openapi
 * components:
 *  schemas:
 *    Mid:    # Schema name
 *      type: object
 *      properties:
 *        mId:
 *          type: string
 *          example: AUD0
 */
export default class Mid {
  /**
   *
   * @param {String} manufacturerId
   * @param {String} modelId
   */
  constructor(manufacturerId, modelId) {
    this.manufacturerId = manufacturerId;
    this.modelId = modelId;
  }

  /**
   * @returns {String}
   */
  getManufacturerId() {
    return this.manufacturerId;
  }

  /**
   * @returns {String}
   */
  getModelId() {
    return this.modelId;
  }
}
