/**
 * @openapi
 * components:
 *  schemas:
 *    ManufacturerModel:    # Schema name
 *      type: object
 *      properties:
 *        manufacturerId:
 *          type: string
 *          example: AUD0
 *        modelId:
 *          type: string
 *          example: 7000029
 */
export default class ManufacturerModel {
  /**
   *
   * @param {String} manufacturerId
   * @param {String} modelId
   */
  constructor(manufacturerId, modelId) {
    this.manufacturerId = manufacturerId;
    this.modelId = modelId;
  }

  /**
   * @returns {String}
   */
  getManufacturerId() {
    return this.manufacturerId;
  }

  /**
   * @returns {String}
   */
  getModelId() {
    return this.modelId;
  }
}
