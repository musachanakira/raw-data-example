import Response from '../Message/Response/Response';

export default class GetVehicles {
  constructor(vehicles, filesystem, render) {
    this.vehicles = vehicles;
    this.filesystem = filesystem;
    this.render = render;
  }

  /**
     *
     * @param {Request} request
     * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*>}
     */
  async execute(request) {
    const value = await this.vehicles.getByManufacturerAndModel(
      request.getBody().manufacturerId,
      request.getBody().modelId,
    );

    await this.filesystem.save(`manufacturers/${request.getBody().manufacturerId}/${request.getBody().modelId}`, value);

    const response = {
      statusCode: 200,
      body: JSON.stringify({
        message: 'SQS event processed.',
        input: request.getBody(),
      }),
    };

    request.getCallback()(null, response);

    return this.render.render(
      new Response(
        200,
        {
          message: 'Successfully saved manufacturers models',
          event: 'request',
        },
      ),
    );
  }
}
