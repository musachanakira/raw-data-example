import Response from '../Message/Response/Response';

export default class GetManufacturers {

  constructor(manufacturers, modelsQueue, filesystem, render) {
    this.manufacturers = manufacturers;
    this.filesystem = filesystem;
    this.render = render;
    this.modelsQueue = modelsQueue;
  }

  /**
   * @returns {Promise<Source|{body: *, statusCode: *}|ConcatSource|*>}
   */
  async execute() {
    const value = await this.manufacturers.getAll();
    value.data.forEach((dataValue) => this.modelsQueue.queue(dataValue.manufacturer_id));

    // await this.filesystem.save('manufacturers', value);

    return this.render.render(
      new Response(
        200,
        {
          message: value,
        },
      ),
    );
  }
}
