export default class GetMid {
  constructor(vehicles, filesystem, render) {
    this.vehicles = vehicles;
    this.filesystem = filesystem;
    this.render = render;
  }

  /**
   *
   * @param {Request} request
   * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*>}
   */
  async execute(request) {
    const value = await this.vehicles.getByMid(request.getBody().mid);

    await this.filesystem.save(`manufacturers/${value.manufacturer_id}/${value.model_id}/${value.mid}`, value);

    return this.render.renderSuccess(
      200,
      {
        message: 'Successfully saved manufacturers models',
        event: 'request',
      },
    );
  }
}
