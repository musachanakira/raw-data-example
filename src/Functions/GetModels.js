import Response from '../Message/Response/Response';

export default class GetModels {
  constructor(models, vehiclesQueue, filesystem, render) {
    this.models = models;
    this.filesystem = filesystem;
    this.render = render;
    this.vehiclesQueue = vehiclesQueue;
  }

  /**
   * @param {Request} request
   * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*>}
   */
  async execute(request) {
    const value = await this.models.getAll(request.getBody().manufacturerId);
    value.data.forEach(
      (dataValue) => this.vehiclesQueue.queue(value.data.manufacturer_id, dataValue.model_id),
    );

    await this.filesystem.save(`manufacturers/${request.getBody().manufacturerId}`, value);


    const response = {
      statusCode: 200,
      body: JSON.stringify({
        message: 'SQS event processed.',
        input: request,
      }),
    };

    request.getCallback()(null, response);

    return this.render.render(
      new Response(
        200,
        {
          message: 'Successfully saved manufacturers models',
        },
      ),
    );
  }
}
