import Response from '../Message/Response/Response';
import ResponseError from '../Exception/ResponseError';

export default class Render {
  /**
   *
   * @param {ResponseError} error
   * @returns {{body: *, statusCode: *}}
   */
  renderError(error) {
    let response = null;
    if (error instanceof ResponseError) {
      response = new Response(error.statusCode, error.message);
    } else {
      response = new Response(500, error.message);
    }

    return this.render(response);
  }

  /**
   *
   * @param {number} statusCode
   * @param {object} body
   * @returns {{body: *, statusCode: *}}
   */
  renderSuccess(statusCode, body) {
    const response = new Response(statusCode, body);

    return this.render(response);
  }

  /**
     *
     * @param {Response} response
     */
  render(response) {
    return {
      statusCode: response.getStatusCode(),
      body: JSON.stringify(
        response.getBody(),
        null,
        2,
      ),
    };
  }
}
