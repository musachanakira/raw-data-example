class Container {
    static instance = null;

    /**
     *
     * @type {{}}
     */
    services = {};

    register(serviceName, service) {
      this.services[serviceName] = service;
    }

    get(serviceName) {
      return this.services[serviceName];
    }

    static getInstance() {
      if (this.instance === null) {
        this.instance = new Container();
      }

      return this.instance;
    }
}

export default Container.getInstance();
