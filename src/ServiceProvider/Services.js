
import AwsS3 from '../Gateway/FileSystem/AwsS3';
import Manufacturer from '../Repository/Cars/Manufacturer';
import Local from '../Gateway/FileSystem/Local';
import Filesystem from '../Repository/Filesystem/Filesystem';
import GetManufacturers from '../Functions/GetManufacturers';
import GetVehicles from '../Functions/GetVehicles';
import Render from '../Render/Render';
import GetModels from '../Functions/GetModels';
import Http from '../Gateway/Http/Http';
import Model from '../Repository/Cars/Model';
import Vehicles from '../Repository/Cars/Vehicles';
import Container from './Container';
import Models from '../Repository/Queue/Models';
import VehiclesQueue from '../Repository/Queue/Vehicles';
import AwsSqs from '../Gateway/Queue/AwsSqs';
import TechnicalInfo from '../Repository/Queue/TechnicalInfo';

class Services {
    static instance = null;

    constructor() {
      Services.createServices();
    }

    static createServices() {
      Container.register('Gateway.FileSystem.AwsS3', new AwsS3());
      Container.register('Gateway.FileSystem.Local', new Local());
      Container.register('Gateway.Http.Http', new Http());
      Container.register('Gateway.Queue.AwsSqs', new AwsSqs());
      Container.register(
        'Repository.Cars.Manufacturer',
        new Manufacturer(Container.get('Gateway.Http.Http')),
      );

      Container.register(
        'Repository.Cars.Model',
        new Model(
          Container.get('Gateway.Http.Http'),
        ),
      );
      Container.register(
        'Repository.Cars.Vehicles',
        new Vehicles(
          Container.get('Gateway.Http.Http'),
        ),
      );
      Container.register(
        'Repository.Cars.Filesystem.Filesystem',
        new Filesystem(
          Container.get('Gateway.FileSystem.AwsS3'),
          Container.get('Gateway.FileSystem.Local'),
        ),
      );
      Container.register(
        'Repository.Queue.Models',
        new Models(
          Container.get('Gateway.Queue.AwsSqs'),
        ),
      );
      Container.register(
        'Repository.Queue.Vehicles',
        new VehiclesQueue(
          Container.get('Gateway.Queue.AwsSqs'),
        ),
      );
      Container.register(
        'Repository.Queue.TechnicalInfo',
        new TechnicalInfo(
          Container.get('Gateway.Queue.AwsSqs'),
        ),
      );
      Container.register(
        'Render.Render',
        new Render(),
      );
      Container.register(
        'Functions.GetManufacturers',
        new GetManufacturers(
          Container.get('Repository.Cars.Manufacturer'),
          Container.get('Repository.Queue.Models'),
          Container.get('Repository.Cars.Filesystem.Filesystem'),
          Container.get('Render.Render'),
        ),
      );
      Container.register(
        'Functions.GetModels',
        new GetModels(
          Container.get('Repository.Cars.Model'),
          Container.get('Repository.Queue.Vehicles'),
          Container.get('Repository.Cars.Filesystem.Filesystem'),
          Container.get('Render.Render'),
        ),
      );
      Container.register(
        'Functions.GetVehicles',
        new GetVehicles(
          Container.get('Repository.Cars.Vehicles'),
          Container.get('Repository.Cars.Filesystem.Filesystem'),
          Container.get('Render.Render'),
        ),
      );
    }

    get(serviceName) {
      return Container.get(serviceName);
    }

    static getInstance() {
      if (this.instance === null) {
        this.instance = new Services();
      }

      return this.instance;
    }
}

export default Services.getInstance();
