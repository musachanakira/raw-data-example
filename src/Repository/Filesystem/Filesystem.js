export default class Filesystem {
  constructor(awsS3, localFs) {
    this.fsAwsS3 = awsS3;
    this.fsLocal = localFs;
  }

  /**
     *
     * @param filename
     * @param content
     * @returns {*|Promise<PromiseResult<D, E>>|Promise<void>|void}
     */
  save(filename, content) {
    if (process.env.environment === 'dev') {
      return this.fsLocal.save(filename, content);
    }
    return this.fsAwsS3.save(filename, content);
  }
}
