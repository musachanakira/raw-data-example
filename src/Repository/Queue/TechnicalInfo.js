
export default class TechnicalInfo {
    /**
     * @var {AwsSqs}
     */
    gatewaySqs;

    /**
     *
     * @param gatewaySqs
     */
    constructor(gatewaySqs) {
      this.gatewaySqs = gatewaySqs;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    queue(topic, mid, subject) {
      return this.gatewaySqs.sendMessage(process.env.SQS_QUEUE_NAME, {
        topic,
        mid,
        subject,
      });
    }
}
