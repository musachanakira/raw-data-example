
export default class Models {
    /**
     * @var {AwsSqs}
     */
    gatewaySqs;

    /**
     *
     * @param gatewaySqs
     */
    constructor(gatewaySqs) {
      this.gatewaySqs = gatewaySqs;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    queue(manufacturerId) {
      this.gatewaySqs.sendMessage(process.env.SQS_QUEUE_NAME, { manufacturerId });
    }
}
