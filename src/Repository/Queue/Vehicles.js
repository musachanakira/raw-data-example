
export default class Vehicles {
    /**
     * @var {AwsSqs}
     */
    gatewaySqs;

    /**
     *
     * @param gatewaySqs
     */
    constructor(gatewaySqs) {
      this.gatewaySqs = gatewaySqs;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    queue(manufacturerId, modelId) {
      this.gatewaySqs.sendMessage(process.env.SQS_QUEUE_NAME, { manufacturerId, modelId });
    }
}
