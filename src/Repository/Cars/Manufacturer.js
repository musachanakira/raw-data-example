export default class Manufacturer {
    /**
     * @var {Http}
     */
    gateway;

    /**
     *
     * @param gateway
     */
    constructor(gateway) {
      this.gateway = gateway;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    getAll() {
      return this.gateway.get('manufacturers');
    }
}
