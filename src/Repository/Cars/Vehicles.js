export default class Vehicles {
    /**
     * @var {Http}
     */
    gateway;

    /**
     *
     * @param gateway
     */
    constructor(gateway) {
      this.gateway = gateway;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    getByManufacturerAndModel(manufacturerId, modelId) {
      return this.gateway.get('vehicles', { manufacturer_id: manufacturerId, model_id: modelId });
    }

    getByMid(mid) {
      return this.gateway.get(`vehicles/${mid}`, {});
    }
}
