
import AWS from 'aws-sdk';// eslint-disable-line import/no-extraneous-dependencies
// Set the region
AWS.config.update({ region: process.env.REGION });

const s3 = new AWS.S3({ apiVersion: '2006-03-01' });

export default class AwsS3 {
  /**
   *
   * @param {string} relativePath
   * @param {Object} body
   * @returns {Promise<PromiseResult>}
   */
  static save(relativePath, body) {
    const fileName = `${relativePath}.json`;
    return s3.putObject({
      Bucket: process.env.BUCKET,
      Key: fileName,
      Body: JSON.stringify(body),
    }).promise();
  }
}
