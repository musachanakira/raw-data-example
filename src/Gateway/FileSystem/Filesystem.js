
export default class Filesystem {
  /**
   *
   * @param {string} relativePath
   * @param {Object} body
   * @returns {Promise<PromiseResult>}
   */
  save(relativePath, body) {
    throw new TypeError('Please extend class accordingly');
  }
}
