
import fs from 'fs';
import Filesystem from './Filesystem';

export default class Local extends Filesystem {
  /**
     *
     * @param fileName
     * @param content
     * @returns {Promise<PromiseResult>}
     */
  static async save(fileName, content) {
    return fs.writeFile(`${fileName}.json`, JSON.stringify(content), () => {});
  }
}
