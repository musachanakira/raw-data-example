import util from 'util';
import AWS from 'aws-sdk';// eslint-disable-line import/no-extraneous-dependencies
// Set the region
// Create an SQS service object
const sqs = new AWS.SQS({ apiVersion: '2012-11-05', region: process.env.REGION });


export default class AwsSqs {
    queues = {};

    /**
     * @param ms
     * @returns {Promise<any>|Promise<void>|*}
     */
    sleep(ms) {
      return util.promisify(setTimeout)(ms);
    }

    async getQueueUrl(queueName) {
      const params = {
        QueueName: queueName,
      };

      let dataResponse = null;
      sqs.getQueueUrl(params, (err, data) => {
        if (err) {
          // Error happened here
        } else {
          dataResponse = data;
        } // successful response
      });

      while (dataResponse === null) {
        // eslint-disable-next-line no-await-in-loop
        await this.sleep(25);
      }

      this.queues[queueName] = dataResponse.QueueUrl;

      return dataResponse.QueueUrl;
    }

    async sendMessage(queueName, body) {
      const params = {
        MessageBody: JSON.stringify(body),
        QueueUrl: await this.getQueueUrl(queueName),
      };

      sqs.sendMessage(params, (err, data) => {
        if (err) {
          // an error occurred
        } else if (data) {
          // successful response
        } else {
          // successful response
        }
      });
    }
}
