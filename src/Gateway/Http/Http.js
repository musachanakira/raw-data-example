import fetch from 'node-fetch';

export default class Http {
    host = 'https://api.autodata-group.com/pre-prod';

    language = 'en-gb';

    countryCode = 'gb';

    format = 'application/json';

    apiKey = 'mstzq7r9bftdejbm6u5kk46a';

    /**
     *
     * @param request
     * @param userParams
     * @returns {Promise<string>}
     */
    get(request, input) {
      const userParams = input || {};
      const defaultParams = {
        'country-code': this.countryCode,
        language: this.language,
        format: this.format,
        api_key: this.apiKey,
      };
      Object.keys(userParams).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(defaultParams, key) === false) {
          defaultParams[key] = userParams[key];
        }
      });

      const params = Object.keys(defaultParams).map((k) => `${encodeURIComponent(k)}=${encodeURIComponent(defaultParams[k])}`).join('&');

      const fullUrl = `${this.host}/v1/${request}?${params}`;

      return fetch(fullUrl)
        .then((result) => result.json())
        .then((result) => result);
    }
}
