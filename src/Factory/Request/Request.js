import RequestMessage from '../../Message/Request/Request';
import ResponseError from '../../Exception/ResponseError';

export default class Request {
  /**
   *
   * @param event
   * @param context
   * @param callback
   * @returns {Request|*}
   */
  makeFromEvent(event, context, callback) {
    if (typeof event !== 'object' && typeof event.Records !== 'undefined' && event.Records.length > 0) {
      return this.makeFromSQS(event, context, callback);
    }

    return this.make({}, context, callback);

    throw new ResponseError('Event given not supported');
  }

  /**
   *
   * @param event
   * @param context
   * @param callback
   * @returns {Request|*}
   */
  makeFromSQS(event, context, callback) {
    return this.make(event.Records[0].body, context, callback);
  }

  /**
   * @param body
   * @param context
   * @param callback
   * @returns {Request}
   */
  make(body, context, callback) {
    return new RequestMessage(body, context, callback);
  }
}
