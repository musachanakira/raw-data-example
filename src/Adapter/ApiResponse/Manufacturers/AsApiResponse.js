import ApiResponse from '../ApiResponse';

export default class AsApiResponse extends ApiResponse {
  toString(json) {
    return json;
  }
}
