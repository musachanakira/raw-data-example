import ApiResponse from '../ApiResponse';

export default class AsJsonFileResponse extends ApiResponse {
  toString(json) {
    const output = json;

    json.data.forEach((manufacturerData, index) => {
      output.data[index] = manufacturerData;
    });
    output.metadata = {};

    return output;
  }
}
