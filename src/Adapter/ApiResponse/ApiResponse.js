
export default class ApiResponse {
  /**
   * Convert Json Response to File Save-able response
   *
   * @param {object} json
   * @returns {*}
   */
  toString() {
    throw new TypeError('Not implemented');
  }
}
