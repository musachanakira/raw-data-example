import Services from '../src/ServiceProvider/Services';
import RequestFactory from '../src/Factory/Request/Request';

const request = new RequestFactory();
const renderer = Services.get('Render.Render');

/**
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*|int>}
 *
 * @openapi
 * getManufacturers:
 *  get:
 *    operationId: GetManufacturers
 *    description: Get manufacturers from V1
 *    tags:
 *     - Lambda
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            required:
 *                - language
 *                - country-code
 *                - format
 *    responses:
 *      '200':
 *        description: Successfully saved all manufacturers list and queue each manufacturer.
 *        content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ManufacturerModel'
 */
export async function getManufacturers(event, context, callback) {
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
  try {
    return Services.get('Functions.GetManufacturers').execute(request.makeFromEvent(event, context, callback));
  } catch (e) {
    return renderer.renderError(e);
  }
}

/**
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*|int>}
 *
 * @openapi
 * getModels:
 *  get:
 *    operationId: getModels
 *    description: Get models from V1
 *    tags:
 *     - Lambda
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            required:
 *                - language
 *                - country-code
 *                - format
 *    responses:
 *      '200':
 *        description: Successfully saved all models list and queue each models.
 *        content:
 *           application/json:
 *             schema:
 *               type: object
   *               $ref: '#/components/schemas/ManufacturerModel'
 */
export async function getModels(event, context, callback) {
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
  try {
    return Services.get('Functions.GetModels').execute(request.makeFromEvent(event, context, callback));
  } catch (e) {
    return renderer.renderError(e);
  }
}

/**
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*|int>}
 *
 * @openapi
 * getVehicles:
 *  get:
 *    operationId: getVehicles
 *    description: Get vehicles from V1
 *    tags:
 *     - Lambda
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            required:
 *                - language
 *                - country-code
 *                - format
 *    responses:
 *      '200':
 *        description: Successfully saved all vehicle variants list and queue each mid.
 *        content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ManufacturerModel'
 */
export async function getVehicles(event, context, callback) {
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
  try {
    return Services.get('Functions.GetVehicles').execute(request.makeFromEvent(event, context, callback));
  } catch (e) {
    return renderer.renderError(e);
  }
}


/**
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<ConcatSource|{body: *, statusCode: *}|Source|*|int>}
 *
 * @openapi
 * getMid:
 *  get:
 *    operationId: GetMid
 *    description: Get mid from V1
 *    tags:
 *     - Lambda
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            required:
 *                - language
 *                - country-code
 *                - format
 *    responses:
 *      '200':
 *        description: Successfully saved all vehicle variants list and queue each technical info.
 *        content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/ManufacturerModel'
 */
export async function GetMid(event, context, callback) {
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
  try {
    return Services.get('Functions.GetMid').execute(request.makeFromEvent(event, context, callback));
  } catch (e) {
    return renderer.renderError(e);
  }
}
