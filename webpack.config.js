const path = require('path');
const slsw = require('serverless-webpack');
require('core-js');

module.exports = {
    mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
    entry: slsw.lib.entries,
    target: 'node',
    module: {
        rules: [
            {
                test: /\.js$/,
        exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    }
                ],
            }
        ]
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, '.webpack'),
        filename: '[name].js'
    }
};